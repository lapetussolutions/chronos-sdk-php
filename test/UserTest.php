<?php

use LapetusSolutions\Chronos;

$config = json_decode(getenv("CHRONOS_TEST_CONFIG"), true);

$chronos_instance = new Chronos\Chronos($config);

$created_props = [
    "id" => "example_id",
    "auth_token" => "example_auth_token",
];
$default_props = [];

class UserTest extends PHPUnit_Framework_TestCase {
    public function testCreateInstance() {
        global $chronos_instance, $created_props;

        $target_props = array_merge([], $created_props);
        $instance = new Chronos\User($chronos_instance, $created_props);
        $this->assertTrue($instance instanceof Chronos\User);
        $this->assertEquals($target_props, $instance->props);
        $this->assertEquals($instance->_chronos, $chronos_instance);
        $this->assertTrue($instance->isCreated());

        $target_props = [];
        $instance = new Chronos\User($chronos_instance, []);
        $this->assertTrue($instance instanceof Chronos\User);
        $this->assertEquals($target_props, $instance->props);
        $this->assertEquals($instance->_chronos, $chronos_instance);
        $this->assertFalse($instance->isCreated());
    }

    public function testAuthMethods() {
        global $chronos_instance, $created_props;

        $auth_options = [
            "some_key" => "some_val",
            "another_key" => "another_val",
        ];

        Chronos\Modifiers\AuthModifier::$methods["new"] = "\AuthNewModifier";

        foreach (["default", "new"] as $key) {
            $instance = new Chronos\User($chronos_instance, $created_props);
            $instance->setAuthMethod($key, $auth_options);
            $this->assertInstanceOf(Chronos\Modifiers\AuthModifier::$methods[$key], $instance->auth);
            $this->assertArraySubset($auth_options, $instance->auth->options);
        }
    }

    public function testNormalizeRequestOptions() {
        $path = "/some/path";
        $this->assertArraySubset([ "path" => $path ], Chronos\User::normalizeRequestOptions($path));
    }

    public function imageErrors() {
        $correct_face = [ "resolution" => true, "contrast" => true ];
        $correct_image = [ "focus" => true, "contrast" => true ];
        return [
            [
                "",
                ["UNKNOWN"]
            ],
            [
                [],
                ["UNKNOWN"]
            ],
            [
                [ "details" => [] ],
                ["NO_FACE"]
            ],
            [
                [ "details" => [ "face" => [ "faces" => 2 ] ] ],
                ["MULTIPLE_FACES"]
            ],
            [
                [ "details" => [ "face" => [ "resolution" => false ] ] ],
                ["LOW_RESOLUTION"]
            ],
            [
                [ "details" => [ "face" => [ "resolution" => true ], "image" => [] ] ],
                ["BAD_FOCUS", "BAD_CONTRAST"]
            ],
            [
                [ "details" => [ "face" => array_merge($correct_face, [ "openmouth" => true ]), "image" => $correct_image ] ],
                ["OPEN_MOUTH"]
            ],
            [
                [ "details" => [ "face" => array_merge($correct_face, [ "closedeyes" => [ "left" => true ] ]), "image" => $correct_image ] ],
                ["CLOSED_EYE"]
            ],
            [
                [ "details" => [ "face" => array_merge($correct_face, [ "closedeyes" => [ "right" => true ] ]), "image" => $correct_image ] ],
                ["CLOSED_EYE"]
            ],
            [
                [ "details" => [ "face" => $correct_face, "FaceAttributes" => [[ "Glasses" => true ]], "image" => $correct_image ] ],
                ["GLASSES"]
            ],
            [
                [ "details" => [ "face" => array_merge($correct_face, [ "pose" => [ "yaw" => 50 ] ]), "image" => $correct_image ] ],
                ["BAD_POSE"]
            ],
            [
                [ "details" => [ "face" => $correct_face, "image" => $correct_image, "celebrities" => [ "0" => [ "name" => "Some Celebrity" ] ] ] ],
                ["CELEBRITY"]
            ],
            [
                [ "details" => [ "face" => $correct_face, "image" => $correct_image ] ],
                ["UNKNOWN"]
            ],
        ];
    }

    /**
     * @dataProvider imageErrors
     */
    public function testGetImageErrors($data, $error_keys) {
        global $chronos_instance, $default_props;
        $instance = new Chronos\User($chronos_instance, $default_props);

        $image_errors = [
            "UNKNOWN" => "The upload failed for an unknown reason",
            "NO_FACE" => "We were unable to find a face in the image.",
            "MULTIPLE_FACES" => "More than one face was found in the image. Please take a picture where you are the only subject!",
            "LOW_RESOLUTION" => "Image is too small.",
            "BAD_FOCUS" => "The image may be too blurry to process. Please try using the auto focus feature of your camera.",
            "BAD_CONTRAST" => "The image may be too dark or too bright to process. Please try again in an area with better lighting.",
            "OPEN_MOUTH" => "You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth.",
            "CLOSED_EYE" => "One or both of your eyes may be closed. Please try again.",
            "GLASSES" => "You might be wearing glasses. Please try again after removing your glasses.",
            "BAD_POSE" => "You may not be looking directly at the camera. Please try again facing directly into the camera.",
            "CELEBRITY" => "Are you really Some Celebrity? Make sure you are uploading a selfie of YOU!",
        ];

        $this->assertEquals($error_keys, $instance->getImageErrors($data));

        $errors = [];
        foreach ($error_keys as $key) {
            $errors[] = $image_errors[$key];
        }

        $this->assertEquals($errors, $instance->getImageErrors($data, true));
    }

    /**
     * @expectedException \LapetusSolutions\Chronos\Exceptions\User\UserNotCreated
     */
    public function testNotCreated() {
        global $chronos_instance;
        $instance = new Chronos\User($chronos_instance);
        $instance->get('/');
    }

    # everything after this point will make requests to the apiroot endpoint you pass in the config

    /**
     * @expectedException \LapetusSolutions\Chronos\Exceptions\User\UserAlreadyCreated
     */
    public function testCreateAlreadyCreated() {
        global $chronos_instance, $created_props;
        $instance = new Chronos\User($chronos_instance, $created_props);
        $instance->create();
    }

    public function createUser() {
        global $chronos_instance, $default_props;
        $instance = new Chronos\User($chronos_instance);

        $target_props = array_merge([
            "appid" => $instance->_chronos->props["appid"]
        ], $default_props);
        $instance->create($default_props);

        $this->assertTrue($instance->isCreated());
        return $instance;
    }

    public function testCreate() {
        $instance = $this->createUser();
    }

    public function testGetUser() {
        $instance = $this->createUser();
        $request_options = [];
        $instance->requestOptions($request_options);
        $this->assertArraySubset([ "id" => $instance->props["id"] ], $instance->getUser([
            "params" => [
                "compact" => false
            ]
        ])["json"]);
    }

    public function testChronosCreateUser() {
        global $chronos_instance;
        $data = [
            "gender" => rand(0, 1) ? "male" : "female",
            "height" => rand(48, 78),
            "weight" => rand(80, 200),
        ];
        $instance = $chronos_instance->createUser($data);
        $this->assertTrue($instance instanceof Chronos\User);
        $this->assertEquals($instance->_chronos, $chronos_instance);
        $this->assertArraySubset($data, $instance->getUser()["json"]);
    }

    public function testChronosUpdateUser() {
        $instance = $this->createUser();
        global $chronos_instance;
        $data = [
            "gender" => rand(0, 1) ? "male" : "female",
            "height" => rand(48, 78),
            "weight" => rand(80, 200),
        ];
        $instance->updateUser($data);
        $this->assertArraySubset($data, $instance->getUser()["json"]);
    }

    /**
     * @expectedException \LapetusSolutions\Chronos\Exceptions\User\ImageValidationFailed
     */
    public function testUploadBadImage() {
        global $config;
        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["incorrect"]);
    }

    public function testUploadGoodImage() {
        global $config;

        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["correct"]);

        $instance = $this->createUser();
        $fh = fopen($config["images"]["correct"], "r");
        $instance->uploadImage($fh);
        fclose($fh);

        # works with multiple images on same instance
        $fh = fopen($config["images"]["correct"], "r");
        $instance->uploadImage(null, fread($fh, fstat($fh)["size"]), $image_key="second");
        fclose($fh);
    }

    public function testGetRawImageDeprecated() {
        global $config;
        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["correct"]);
        $this->assertTrue(strlen($instance->getRawImage()["text"]) > 0);
    }

    public function testGetRawImage() {
        global $config;
        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["correct"]);
        $this->assertTrue(strlen($instance->getRawImage()["text"]) > 0);
    }

    /**
     * @expectedException \LapetusSolutions\Chronos\Exceptions\RequestException
     */
    public function testDeleteImages() {
        global $config;

        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["correct"]);
        $this->assertTrue(strlen($instance->getRawImage()["text"]) > 0);

        $instance->deleteImages();
        $instance->getRawImage();
    }

    /**
     * @expectedException \LapetusSolutions\Chronos\Exceptions\RequestException
     */
    public function testDeleteUser() {
        global $config;
        $instance = $this->createUser();
        $instance->uploadImage($config["images"]["correct"]);
        $this->assertTrue(strlen($instance->getRawImage()["text"]) > 0);

        $instance->deleteUser();
        $this->assertFalse(strlen($instance->getRawImage()["text"]) > 0);
    }

    public function testEstimations() {
        global $config;

        $instance = $this->createUser();
        $res = $instance->uploadImage($config["images"]["correct"]);

        $complete = false;
        foreach (range(1, 10) as $i) {
            $estimations = $instance->getEstimations()["json"];
            if (isset($estimations["gender"]["details"]["value"])
                && isset($estimations["chronage"]["details"]["value"])
                && isset($estimations["bmi"]["details"]["value"])
                && isset($estimations["smoker"]["details"]["value"])
            ) {
                $complete = true;
                break;
            }
            sleep(1);
        }
        $this->assertTrue($complete);
    }

    public function testLifespan() {
        global $config;
        $instance = $this->createUser();

        $age = rand(18, 95);
        $dob = date("Ymd", strtotime("-$age years"));

        $instance->updateUser([
            "gender" => rand(0, 1) ? "male" : "female",
            "dob" => $dob,
        ]);

        $compact_keys = ["baseline", "adjusted", "vbt_baseline"];
        $full_keys = array_merge($compact_keys, ["age", "baseline_survival", "baseline_survival_error", "adjusted_survival", "adjusted_survival_error", "vbt_baseline_survival", "vbt_baseline_survival_error", "adjusted_deathrate"]);

        $compact_lifespan = $instance->getLifespan([
            "params" => [
                "compact" => "true"
            ]
        ])["json"];
        $full_lifespan = $instance->getLifespan()["json"];

        foreach ($compact_keys as $key) {
            $this->assertArrayHasKey($key, $compact_lifespan);
        }
        foreach ($full_keys as $key) {
            $this->assertArrayHasKey($key, $full_lifespan);
        }
    }
}
