<?php

/*
export CHRONOS_TEST_CONFIG="{\"apiroot\":\"https://[chronos_domain]/v1\",\"appid\":\"[appid]\",\"orgid\":\"[orgid]\",\"apikey\":\"[apikey]\",\"images\":{\"correct\":\"[path/to/image]\",\"incorrect\":\"[path/to/image]\"}}"
export CHRONOS_TEST_CONFIG="{\"apiroot\":\"https://chronos.lapetussolutions.com/v1\",\"appid\":\"chronos-sdk-python-test\",\"orgid\":\"lapetussolutions\",\"apikey\":\"sMuRXQzkPycu4GWPVx2T50ATKPnb1cfV\",\"images\":{\"correct\":\"/Users/daniel/Pictures/test-images/Samuel L Jackson 1.png\",\"incorrect\":\"/Users/daniel/Pictures/test-images/people.jpeg\"}}"
./vendor/bin/phpunit test
*/

$config = json_decode(getenv("CHRONOS_TEST_CONFIG"), true);

use LapetusSolutions\Chronos;

class AuthNewModifier extends Chronos\RequestModifier {
}

$additional_headers = [
    "Some-Header" => "Some-Value",
    "Another-Header" => "Another-Value",
];

class SetAdditionalHeadersModifier extends Chronos\RequestModifier {
    public function requestOptions($instance, &$request_options, $options = []) {
        global $additional_headers;
        if (!isset($request_options["headers"])) {
            $request_options["headers"] = [];
        }
        foreach ($additional_headers as $key => $value) {
            $request_options["headers"][$key] = $value;
        }
    }
}
function set_additional_headers_modifier($instance, &$request_options, $options = []) {
    global $additional_headers;
    if (!isset($request_options["headers"])) {
        $request_options["headers"] = [];
    }
    foreach ($additional_headers as $key => $value) {
        $request_options["headers"][$key] = $value;
    }
}


class ChronosTest extends PHPUnit_Framework_TestCase {
    public function testCreateInstance() {
        $target_props = [
            "apikey" => '',
            "apiroot" => '',
            "appid" => '',
            "orgid" => '',
        ];
        $instance = new Chronos\Chronos($target_props);
        $this->assertEquals($target_props, $instance->props);
        $this->assertTrue($instance instanceof Chronos\Chronos);
    }

    public function testAuthMethods() {
        global $config;

        $auth_options = [
            "some_key" => "some_val",
            "another_key" => "another_val",
        ];

        Chronos\Modifiers\AuthModifier::$methods["new"] = "\AuthNewModifier";

        foreach (["default", "new"] as $key) {
            $instance = new Chronos\Chronos($config);
            $instance->setAuthMethod($key, $auth_options);
            $this->assertInstanceOf(Chronos\Modifiers\AuthModifier::$methods[$key], $instance->auth);
            $this->assertArraySubset($auth_options, $instance->auth->options);
        }
    }

    public function modifierTests() {
        return [
            [[
                "pre" => [new SetAdditionalHeadersModifier()],
            ]],
            [[
                "post" => [new SetAdditionalHeadersModifier()],
            ]],
            [[
                "pre" => ["\set_additional_headers_modifier"],
            ]],
            [[
                "post" => ["\set_additional_headers_modifier"],
            ]]
        ];
    }

    /**
     * @dataProvider modifierTests
     */
    public function testModifiers($modifiers) {
        global $config, $additional_headers;

        $modifier_key = array_keys($modifiers)[0];
        $modifier_values = $modifiers[$modifier_key];

        $instance = new Chronos\Chronos($config);

        $request_options = [];

        $instance->requestOptions($request_options, [
            "modifiers" => $modifiers,
        ]);

        $this->assertArraySubset($additional_headers, $request_options["headers"]);

        if (count($modifier_values) == 1) {
            $request_options = [];
            Chronos\Chronos::applyRequestModifier($modifier_values[0], $instance, $request_options);
            $this->assertArraySubset($additional_headers, $request_options["headers"]);
        }

        $request_options = [];
        Chronos\Chronos::applyRequestModifiers($modifier_values, $instance, $request_options);
        $this->assertArraySubset($additional_headers, $request_options["headers"]);

        $instance = new Chronos\Chronos($config);
        $instance->modifiers[$modifier_key] = array_merge($instance->modifiers[$modifier_key], $modifier_values);
        $request_options = [];
        $instance->requestOptions($request_options);
        $this->assertArraySubset($additional_headers, $request_options["headers"]);
    }

    public function testNormalizeRequestOptions() {
        $path = "/some/path";
        $this->assertArraySubset([ "path" => $path ], Chronos\Chronos::normalizeRequestOptions($path));
    }

    public function testHref() {
        global $config;
        $instance = new Chronos\Chronos($config);

        $this->assertEquals("/o/" . $instance->props["orgid"], $instance->orgHref());
        $this->assertEquals("/o/" . $instance->props["orgid"] . "/a/" . $instance->props["appid"], $instance->appHref());
    }

    public function urlTests() {
        global $config;
        $instance = new Chronos\Chronos($config);

        $base = trim($instance->props["apiroot"], "/") . "/" . trim($instance->appHref(), "/");
        $custom = "https://custom-url";
        $path = "/some/path";

        $tests = [];

        $tests[] = [[], [ "url" => $base ]];

        foreach ([$base, "$base/$custom", "$custom/"] as $url) {
            $tests[] = [[ "url" => $url ], [ "url" => $url ]];
            foreach ([$path, "$path/"] as $path) {
                $tests[] = [[
                    "url" => $url,
                    "path" => $path,
                ], [
                    "url" => trim($url, "/") . "/" . trim($path, "/")
                ]];
            }
        }

        return $tests;
    }

    /**
     * @dataProvider urlTests
     */
    public function testUrl($request_options, $target_request_options) {
        global $config;
        $instance = new Chronos\Chronos($config);
        $modifier = new Chronos\Modifiers\UrlModifier();
        Chronos\Chronos::applyRequestModifier($modifier, $instance, $request_options);
        $this->assertArraySubset($target_request_options, $request_options);
    }

    public function headerTests() {
        global $config;
        $instance = new Chronos\Chronos($config);

        $base = [
            "Authorization" => "apikey {$instance->props["apikey"]}",
        ];
        $data = [
            "test" => "ing"
        ];
        $base_headers = [
            "Content-Type" => "application/json",
            "Accept" => "application/json",
            "Content-Length" => strlen(json_encode($data)),
        ];
        $base_headers["User-Agent"] = "chronos-sdk-php/1.0.0";
        $base_headers["User-Agent"] .= " (";
        $base_headers["User-Agent"] .= "PHP " . phpversion();
        $base_headers["User-Agent"] .= "; cURL " . curl_version()["version"];
        $base_headers["User-Agent"] .= ")";
        $additional = [
            "Additional-Header" => "Value"
        ];
        global $modifier_headers;
        $modifier_headers = [
            "Modifier-Header" => "Modifier-Value"
        ];

        $modifier = function ($instance, &$request_options, $options = []) {
            global $modifier_headers;
            if (!isset($request_options["headers"])) {
                $request_options["headers"] = [];
            }
            $request_options["headers"] = array_merge($request_options["headers"], $modifier_headers);
        };

        $instance->modifiers["pre"][] = $modifier;

        return [
            [
                new Chronos\Chronos($config),
                [
                    "data" => $data,
                ],
                [
                    "headers" => array_merge($base, $base_headers),
                ]
            ],
            [
                $instance,
                [
                    "data" => $data,
                ],
                [
                    "headers" => array_merge($base, $base_headers, $modifier_headers),
                ]
            ],
            [
                $instance,
                [
                    "data" => $data,
                    "headers" => $additional,
                ],
                [
                    "headers" => array_merge($base, $base_headers, $modifier_headers, $additional),
                ]
            ],
        ];
    }

    /**
     * @dataProvider headerTests
     */
    public function testHeaders($instance, $request_options, $target_request_options) {
        $instance->requestOptions($request_options);
        $this->assertArraySubset($target_request_options["headers"], $request_options["headers"]);
    }

    public function queryStringTests() {
        global $config;
        $instance = new Chronos\Chronos($config);

        $base_params = [
            "compact" => "true"
        ];
        $additional = [
            "Additional-Param" => "Value"
        ];
        global $modifier_params;
        $modifier_params = [
            "Modifier-Param" => "Modifier-Value"
        ];

        $modifier = function ($instance, &$request_options, $options = []) {
            global $modifier_params;
            if (!isset($request_options["params"])) {
                $request_options["params"] = [];
            }
            $request_options["params"] = array_merge($request_options["params"], $modifier_params);
        };

        $instance->modifiers["pre"][] = $modifier;

        return [
            [
                new Chronos\Chronos($config),
                [
                    "data" => $data,
                ],
                [
                    "params" => $base_params,
                ]
            ],
            [
                $instance,
                [
                    "data" => $data,
                ],
                [
                    "params" => array_merge($base_params, $modifier_params),
                ]
            ],
            [
                $instance,
                [
                    "data" => $data,
                    "params" => $additional,
                ],
                [
                    "params" => array_merge($base_params, $modifier_params, $additional),
                ]
            ],
        ];
    }

    /**
     * @dataProvider queryStringTests
     */
    public function testQueryString($instance, $request_options, $target_request_options) {
        $instance->requestOptions($request_options);
        $this->assertArraySubset($target_request_options["params"], $request_options["params"]);
    }

    public function testUser() {
        global $config;
        $instance = new Chronos\Chronos($config);
        $user = $instance->user();
        $this->assertTrue($user instanceof Chronos\User);
        $this->assertEquals($user->_chronos, $instance);
        $this->assertFalse($user->isCreated());

        $user = $instance->user([
            "id" => ""
        ]);
        $this->assertFalse($user->isCreated());

        $user = $instance->user([
            "id" => "[id]"
        ]);
        $this->assertTrue($user->isCreated());
    }
}
