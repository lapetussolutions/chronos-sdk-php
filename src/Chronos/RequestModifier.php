<?php

namespace LapetusSolutions\Chronos;

/**
 * Base class for making custom request modifiers
 */
class RequestModifier {
    /**
     * (`array`) Request modifier options
     */
    public $options;

    /**
     * @param array $options Request modifier options
     */
    public function __construct($options = []) {
        $this->options = $options;
    }

    /**
     * Method to be overwritten to modify requests
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
    }
}
