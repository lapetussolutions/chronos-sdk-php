<?php

namespace LapetusSolutions\Chronos\Exceptions\User;

class UserAlreadyCreated extends \Exception {
    /**
     * Exception used for indicating a user has been created
     */

    public function __construct($message, $code = 0, Exception $previous = NULL) {
        parent::__construct($message, $code, $previous);
    }
}
