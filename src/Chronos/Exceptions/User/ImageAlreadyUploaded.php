<?php

namespace LapetusSolutions\Chronos\Exceptions\User;

use LapetusSolutions\Chronos\Exceptions;

class ImageAlreadyUploaded extends Exceptions\RequestException {
    /**
     * Exception used for indicating a user already has an image upload
     */

    public function __construct($message, $response, $code = 0, $previous = NULL) {
        parent::__construct($message, $response, $code, $previous);
    }
}
