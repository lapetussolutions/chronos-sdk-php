<?php

namespace LapetusSolutions\Chronos\Exceptions\User;

use LapetusSolutions\Chronos\Exceptions;

class ImageValidationFailed extends Exceptions\RequestException {
    /**
     * Exception used for indicating a user has not been created
     */

    public function __construct($message, $response, $code = 0, $previous = NULL) {
        parent::__construct($message, $response, $code, $previous);
    }
}
