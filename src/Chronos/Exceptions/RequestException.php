<?php

namespace LapetusSolutions\Chronos\Exceptions;

class RequestException extends \Exception {
    /**
     * Exception used for request errors
     */

    /**
     * @var array Response array (see `Chronos::request`)
     */
    public $response;

    public function __construct($message, $response, $code = 0, $previous = NULL) {
        parent::__construct($message, $code, $previous);
        $this->response = $response;
    }
}
