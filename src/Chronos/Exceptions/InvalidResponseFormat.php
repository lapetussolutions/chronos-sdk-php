<?php

namespace LapetusSolutions\Chronos\Exceptions;

class InvalidResponseFormat extends RequestException {
    /**
     * Exception used for indicating an invalid response format
     */

    public function __construct($message, $expected, $response, $code = 0, $previous = NULL) {
        parent::__construct($message, $response, $code, $previous);
        $this->expected = $expected;
    }
}
