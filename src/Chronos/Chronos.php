<?php
namespace LapetusSolutions\Chronos;

require_once("Modifiers/helpers.php");

/**
 * Class for interacting with the Chronos API
 * @version v1.0.0
 */
class Chronos {
    /** (`string`) Chronos API Version */
    public $version = "1.0.0";

    /**
     * (`array`) Properties array containing Chronos API endpoint and credentials:
     *
     * - ['apiroot'] (`string`) - Chronos API root
     * - ['orgid'] (`string`) - Chronos Organization ID
     * - ['apikey'] (`string`) - Chronos Organization API Key
     * - ['appid'] (`string`) - Chronos App ID
     *
     */
    public $props;

    /**
     * (`array`) Configuration array
     *
     * - ['compact_mode'] (`boolean`) - If `true`, all requests will have the "compact=true" query string parameter added. Defaults to `true`
     * - ['user_agent']   (`string`) - User agent to be sent in each request. Defaults to `"chronos-sdk-php/1.0.0 (PHP [PHP version]; cURL [cURL version])"`
     *
     */
    public $config;

    /**
     * @var LapetusSolutions\\Chronos\\RequestModifier|function|string Authentication request modifier. Defaults to `"default"`
     */
    public $auth;

    /**
     * (`array`) Array of default request modifiers
     *
     * - ['pre']  (`array`) - Array of request modifiers to be applied to all requests
     * - ['post'] (`array`) - Array of request modifiers to be applied to all requests after the 'pre' list
     *
     */
    public $modifiers;

    /**
     * Class constructor
     *
     * @param array $props Properties array (see above)
     * @param array $config Configuration array (see above)
     */
    function __construct($props, $config = []) {
        $this->props = (array) $props;
        foreach (["apiroot", "orgid", "apikey", "appid"] as $key) {
            if (!(isset($this->props[$key]) && is_string($this->props[$key]))) {
                $this->props[$key] = "";
            }
        }
        if (substr($this->props["apiroot"], -1) == "/") {
            $this->props["apiroot"] = substr($this->props["apiroot"], 0, -1);
        }

        if (is_object($config)) {
            $config = (array) $config;
        } else if (!is_array($config)) {
            $config = [];
        }
        $this->config = $config;
        if (!isset($this->config["compact_mode"])) {
            $this->config["compact_mode"] = true;
        }
        if (!isset($this->config["user_agent"])) {
            $this->config["user_agent"] = "chronos-sdk-php/" . $this->version;
            $this->config["user_agent"] .= " (";
            $this->config["user_agent"] .= "PHP " . phpversion();
            $this->config["user_agent"] .= "; cURL " . curl_version()["version"];
            $this->config["user_agent"] .= ")";
        }

        $this->setAuthMethod("default");

        $this->modifiers = [
            "pre" => [
                new Modifiers\AuthModifier([ "instance" => $this ]),
                new Modifiers\UserAgentModifier(),
            ],
            "post" => [
                new Modifiers\ContentTypeModifier(),
                new Modifiers\ContentLengthModifier(),
                new Modifiers\AcceptModifier(),
                new Modifiers\CompactModifier(),
                new Modifiers\UrlModifier(),
            ],
        ];
    }

    /**
     * Sets the authentication request modifier
     *
     * @param LapetusSolutions\\Chronos\\RequestModifier|function|string $method Authentication request modifier. Defaults to `"default"`
     * @param array $options Options to be passed to the modifier. Only used if `$method` is a `LapetusSolutions\\Chronos\\RequestModifier` or `string`
     *
     * @throws \\Exception if the $method parameter is invalid
     *
     * @returns null
     */
    function setAuthMethod($method = "default", $options = []) {
        if (is_string($method)) {
            if (isset(Modifiers\AuthModifier::$methods[$method])) {
                $method = Modifiers\AuthModifier::$methods[$method];
                $this->auth = new $method($options);
                return;
            } else if (class_exists($method)) {
                $this->auth = new $method($options);
                return;
            } else if (function_exists($method)) {
                $this->auth = $method;
                return;
            }
        } else if ($method instanceof RequestModifier) {
            $this->auth = $method;
            return;
        } else if (is_callable($method)) {
            $this->auth = $method;
            return;
        }
        throw new \Exception("The `method` property must be an instance of LapetusSolutions\Chronos\RequestModifier, function, or string naming a default auth method key or a class or function");
    }

    /**
     * Applies a modifier to a request options array
     *
     * @param LapetusSolutions\\Chronos\\RequestModifier|function|string $modifier Authentication request modifier
     * @param LapetusSolutions\\Chronos\\Chronos $instance Instance to pass to the modifier
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifier
     */
    static function applyRequestModifier($modifier, $instance, &$request_options, $options = []) {
        if (is_callable($modifier)) {
            $modifier($instance, $request_options, $options);
            return;
        }
        $modifier->requestOptions($instance, $request_options, $options);
    }

    /**
     * Applies an array of modifiers to a request options array
     *
     * @param (LapetusSolutions\\Chronos\\RequestModifier|function)[] $modifiers Authentication request modifiers
     * @param LapetusSolutions\\Chronos\\Chronos $instance Instance to pass to the modifiers
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     */
    static function applyRequestModifiers($modifiers, $instance, &$request_options, $options = []) {
        foreach ($modifiers as $modifier) {
            self::applyRequestModifier($modifier, $instance, $request_options, $options);
        }
    }

    /**
     * Combines an array of modifier objects into an array of modifiers
     *
     * @param array $modifier_objects Array of modifier objects
     * @param array $order Order of modifier array keys. Defaults to [ "pre", "post" ]
     *
     * @return array Array of modifiers
     */
    static function combineModifiers($modifier_objects, $order = [ "pre", "post" ]) {
        $modifiers = [];
        foreach ($order as $key) {
            foreach ($modifier_objects as $modifier_object) {
                if (isset($modifier_object[$key]) && is_array($modifier_object[$key])) {
                    $modifiers = array_merge($modifiers, $modifier_object[$key]);
                }
            }
        }
        return $modifiers;
    }

    /**
     * @return string Path of the Chronos orginization
     */
    function orgHref() {
        return "/o/" . $this->props["orgid"];
    }

    /**
     * @return string Path of the Chronos app
     */
    function appHref() {
        return $this->orgHref() . "/a/" . $this->props["appid"];
    }

    /**
     * Request modifier to create the base url for the orgid and appid in the `$props` property
     *
     * @param LapetusSolutions\\Chronos\\Chronos $instance Chronos instance. This parameter is ignored
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * $request_options
     *
     * - ['url']  (`string`) - Base url for a request. Defaults to the instance's `appHref()`
     * - ['path'] (`string`) - Path to be added to the base url
     *
     */
    function url($instance, &$request_options, $options = []) {
        if (!isset($request_options["url"])) {
            $request_options["url"] = self::urlJoin([$this->props["apiroot"], $this->appHref()]);
        }
        if (empty($request_options["path"]) || !is_string($request_options["path"])) {
            return;
        }
        $request_options["url"] = self::urlJoin([$request_options["url"], $request_options["path"]]);
    }

    /**
     * Creates full request options array
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * $request_options
     *
     * - ['url']  (`string`) - Request base url. Defaults to the instance's `appHref()`
     * - ['path'] (`string`) - Path to be added to the base url
     *
     * $options
     *
     * - ['modifiers'] (`array`) - Modifiers to be combined with the instance's default modifiers and applied to the request options
     * - ['override_modifiers'] (`boolean`) - If `true`, the instance's default modifiers will not be applied to the request options
     */
    function requestOptions(&$request_options, $options = []) {
        if (!isset($request_options["method"])) {
            $request_options["method"] = "get";
        }
        if (!isset($request_options["headers"])) {
            $request_options["headers"] = [];
        }
        if (!isset($request_options["params"])) {
            $request_options["params"] = [];
        }
        if (!isset($request_options["data"])) {
            $request_options["data"] = [];
        }

        $base_modifiers = $this->modifiers;
        if (!empty($options["override_modifiers"])) {
            $base_modifiers = [];
        }

        $modifier_objects = [$base_modifiers];

        if (!empty($options["modifiers"]) && is_array($options["modifiers"])) {
            $modifier_objects[] = $options["modifiers"];
        }

        $modifiers = self::combineModifiers($modifier_objects);

        self::applyRequestModifiers($modifiers, $this, $request_options, $options);
    }

    /**
     * Makes the request options a array if a string is passed
     *
     * @param array $request_options Request options array
     *
     * @return array
     */
    static function normalizeRequestOptions($request_options) {
        if (is_string($request_options)) {
            return [ "path" => $request_options ];
        }
        return $request_options;
    }

    /**
     * Makes a request
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * $request_options
     *
     * - ['method'] (`string`) - Request method. Defaults to "get"
     * - ['url'] (`string`) - Base url for a request. Defaults to the instance's `appHref()`
     * - ['path'] (`string`) - Path to be added to the base url
     * - ['headers'] (`array`) - Request headers
     * - ['params'] (`array`) - Request query string
     * - ['data'] (`mixed`) - Request data
     *
     * $options
     *
     * - ['modifiers'] (`array`) - Modifiers to be combined with the instance's default modifiers and applied to the request options
     * - ['override_modifiers'] (`boolean`) - If `true`, the instance's default modifiers will not be applied to the request options
     *
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request or if the response format is incorrect
     *
     * @return array Assoc array with the following keys: "text" (raw response text), "json" (assoc array of the json decoded response text), "headers" (assoc array of the response headers), and "status" (status code)
     */
    function request($request_options, $options = []) {
        $request_options = self::normalizeRequestOptions($request_options);
        $this->requestOptions($request_options, $options);

        $curl = curl_init();
        $method = strtoupper($request_options["method"]);
        if ($method == "POST") {
            curl_setopt($curl, CURLOPT_POST, 1);
        } else {
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, $method);
        }

        if (isset($request_options["data"]) && is_array($request_options["data"])) {
            $request_options["data"] = json_encode($request_options["data"]);
        }
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request_options["data"]);

        $formatted_headers = [];
        foreach ($request_options["headers"] as $key => $value) {
            $formatted_headers[] = "$key: $value";
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $formatted_headers);

        $url = $request_options["url"];
        if (!empty($request_options["params"]) && is_array($request_options["params"])) {
            $qs_start_pos = strpos($url, "?");
            if ($qs_start_pos === false) {
                $url .= "?";
            } else if ($qs_start_pos < strlen($url) - 1) {
                $url .= "&";
            }
            $url .= http_build_query($request_options["params"]);
        }
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $response_headers = [];
        curl_setopt($curl, CURLOPT_HEADERFUNCTION, function($curl, $header) use (&$response_headers) {
            $len = strlen($header);
            $header = explode(':', $header, 2);
            if (count($header) < 2) {
                return $len;
            }

            $key = strtolower(trim($header[0]));
            $value = trim($header[1]);

            if (!isset($response_headers[$key])) {
                $response_headers[$key] = [];
            }
            $response_headers[$key][] = $value;

            return $len;
        });

        try {
            $result = [];
            $result["text"] = curl_exec($curl);
            $result["status"] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $result["headers"] = $response_headers;

            $result["json"] = json_decode($result["text"], true);
            if ($result["json"] === null) {
                $accept = Modifiers\get_header("Accept", $request_options);
                if ($accept == "application/json") {

                }
                if ($response_format == "json") {
                    throw new Exceptions\InvalidResponseFormat("Response text should be JSON", "json", $result, null, $e);
                }
            }
            if ($result["status"] >= 400 && $result["status"] <= 599) {
                throw new Exceptions\RequestException("The request failed for an unknown reason. Check the response for more details", $result);
            }
        } catch (Exception $e) {
            if (!($e instanceof Exceptions\RequestException)) {
                throw new Exceptions\RequestException("The request failed for an unknown reason. Check the response for more details", $result, null, $e);
            }
        }

        curl_close($curl);
        return $result;
    }

    /**
     * Makes a request using the GET method. See `request` method for more details
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request (see `request`)
     *
     * @return array Response from the request (see `request`)
     */
    function get($request_options, $options = []) {
        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "get";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the POST method. See `request` method for more details
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request (see `request`)
     *
     * @return array Response from the request (see `request`)
     */
    function post($request_options, $options = []) {
        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "post";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the PUT method. See `request` method for more details
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request (see `request`)
     *
     * @return array Response from the request (see `request`)
     */
    function put($request_options, $options = []) {
        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "put";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the DELETE method. See `request` method for more details
     *
     * @param array $request_options Request options array
     * @param array $options Options to be passed to the modifiers
     *
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request (see `request`)
     *
     * @return array Response from the request (see `request`)
     */
    function delete($request_options, $options = []) {
        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "delete";
        return $this->request($request_options, $options);
    }

    /**
     * Create an instance of the User class.
     * If you want to access and/or manipulate a user already created in Chronos, pass the user's `id` and `auth_token` in the $props param
     *
     * @param array|object $props Properties of the Chronos user
     *
     * @return LapetusSolutions\\Chronos\\User Instance of the User class
     */
    function user($props = []) {
        return new User($this, $props);
    }

    /**
     * Creates an instance of the User class, and saves the user in Chronos.
     * Do not use this if the user already exists in Chronos, see the `user` method above
     *
     * @param array $data User data to be sent in the create request
     * @param array $props Properties of the Chronos user
     *
     * @return LapetusSolutions\\Chronos\\User Instance of the User class
     */
    function createUser($data = [], $props = [], $request_options = [], $options = []) {
        $user = $this->user($props);
        $res = $user->create($data, $request_options, $options);
        return $user;
    }
}
