<?php
namespace LapetusSolutions\Chronos;

require_once("Modifiers/helpers.php");

/**
 * Class for interacting with users within the Chronos API
 * @version v1.0.0
 */
class User {
    /**
     * @var LapetusSolutions\\Chronos\\Chronos Chronos instance
     */
    public $_chronos;

    /**
     * @var array User properties
     *
     * $props
     *
     * - ['id'] (`string`) - User ID
     * - ['auth_token'] (`string`) - User API auth token
     *
     */
    public $props;

    /**
     * @var LapetusSolutions\\Chronos\\RequestModifier|function Authentication request modifier
     */
    public $auth;

    /**
     * @var array Array of default request modifiers
     *     $props
     * - ['pre'] (`array`) - Array of request modifiers to be applied to all requests
     * - ['post'] (`array`) - Array of request modifiers to be applied to all requests after the `pre` list
     *
     */
    public $modifiers;

    /**
     * Class constructor
     *
     * @param LapetusSolutions\Chronos\Chronos $chronos Chronos instance
     * @param array $props User properties (see `$props` above). If you want to access and/or manipulate a user already created in Chronos, pass the user's `id` and `auth_token` in this param
     *
     * @return void
     */
    function __construct($chronos, $props = []) {
        $this->_chronos = $chronos;
        $this->props = $props;

        $this->setAuthMethod("default", [
            "authorization_type" => "token",
            "token_key" => "auth_token",
        ]);

        $this->modifiers = [
            "pre" => [
                new Modifiers\AuthModifier([ "instance" => $this ]),
                new Modifiers\User\UrlModifier([ "instance" => $this ]),
            ],
            "post" => [],
        ];
    }

    /**
     * Returns a boolean of whether the user has the `id` property set
     *
     * @return boolean
     */
    function isCreated() {
        return !empty($this->props["id"]);
    }


    /**
     * Throws an exception if the user's created state (`isCreated()`) does not match the `$should_be_created` argument
     *
     * @param boolean $should_be_created Whether or not the user should be created. Defaults to `true`
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if `$should_be_created` is `true` and `isCreated()` is `false`
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserAlreadyCreated if `$should_be_created` is `false` and `isCreated()` is `true`
     */
    function _isCreated($should_be_created = true) {
        $is_created = $this->isCreated();
        if ($should_be_created && !$is_created) {
            throw new Exceptions\User\UserNotCreated("User must be created to use this method");
        } else if (!$should_be_created && $is_created) {
            throw new Exceptions\User\UserAlreadyCreated("User must not be created to use this method");
        }
    }

    /**
     * Sets the authentication request modifier
     *
     * @param LapetusSolutions\Chronos\RequestModifier|function|string $method Authentication request modifier. Defaults to "default"
     * @param array $options Options to be passed to the modifier. Only used if `$method` is a `LapetusSolutions\Chronos\RequestModifier` or `string`
     *
     * @throws Exception if the `$method` argument is invalid
     */
    function setAuthMethod($method = "default", $options = []) {
        if (is_string($method)) {
            if (isset(Modifiers\AuthModifier::$methods[$method])) {
                $method = Modifiers\AuthModifier::$methods[$method];
                $this->auth = new $method($options);
                return;
            } else if (class_exists($method)) {
                $this->auth = new $method($options);
                return;
            } else if (function_exists($method)) {
                $this->auth = $method;
                return;
            }
        } else if ($method instanceof RequestModifier) {
            $this->auth = $method;
            return;
        } else if (is_callable($method)) {
            $this->auth = $method;
            return;
        }
        throw new \Exception("The `method` property must be an instance of LapetusSolutions\Chronos\RequestModifier, function, or string naming a default auth method key or a class or function");
    }

    /**
     * Makes the request options an array if a string is passed (see `Chronos::normalizeRequestOptions`)
     *
     * @param array $request_options Request options array
     *
     * @return array
     */
    static function normalizeRequestOptions($request_options) {
        return Chronos::normalizeRequestOptions($request_options);
    }

    /**
     * Makes a request ("POST /o/[orgid]/a/[appid]/users") to create a user in Chronos
     *
     * @param array $data User data to be sent in the request
     * @param $request_options Request options array (see `Chronos::request`). Optional.
     * @param $options Options to be passed to the request (see `Chronos::request`). Optional.
     *
     * @throws LapetusSolutions\Chronos\Exceptions\UserAlreadyCreated if the user is already created
     *
     * @return array Request response (see `Chronos::request`)
     */
    function create($data = [], $request_options = [], $options = []) {
        $this->_isCreated(false);

        $data = array_merge([
            "appid" => $this->_chronos->props["appid"]
        ], $data);

        $request_options = array_merge($request_options, [
            "method" => "post",
            "path" => "/users",
            "data" => $data
        ]);

        $res = $this->_chronos->request($request_options, $options);
        $this->props = array_merge($this->props, $res["json"]);
        return $res;
    }

    function requestOptions(&$request_options, $options = []) {
        /**
         * Creates full request options array using the user's default modifiers (see `Chronos::requestOptions`)
         *
         * @param array $request_options Request options array
         * @param array $options Options array. This will also be passed to the modifiers
         */
        $modifiers = $this->modifiers;
        if (isset($options["modifiers"]) && is_array($options["modifiers"])) {
            foreach (["pre", "post"] as $key) {
                if (isset($options["modifiers"][$key])) {
                    $modifiers[$key] = array_merge($modifiers[$key], $options["modifiers"][$key]);
                }
            }
        }
        $options["modifiers"] = $modifiers;

        $this->_chronos->requestOptions($request_options, $options);
    }

    function request($request_options, $options = []) {
        /**
         * Makes a request relative to the user's href and applies modifiers in `$this->modifiers`
         *
         * @param array $request_options Request options array (see `Chronos::request`)
         * @param array $options Options to be passed to the request
         *
         * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
         *
         * @return array Request response (see `Chronos::request`)
         */
        $this->_isCreated(true);
        $request_options = self::normalizeRequestOptions($request_options);

        $modifiers = $this->modifiers;
        if (isset($options["modifiers"]) && is_array($options["modifiers"])) {
            foreach (["pre", "post"] as $key) {
                if (isset($options["modifiers"][$key])) {
                    $modifiers[$key] = array_merge($modifiers[$key], $options["modifiers"][$key]);
                }
            }
        }
        $options["modifiers"] = $modifiers;

        return $this->_chronos->request($request_options, $options);
    }

    /**
     * Makes a request using the GET method
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function get($request_options, $options = []) {
        $this->_isCreated(true);

        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "get";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the POST method
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function post($request_options, $options = []) {
        $this->_isCreated(true);

        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "post";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the PUT method
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function put($request_options, $options = []) {
        $this->_isCreated(true);

        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "put";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request using the DELETE method
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function delete($request_options, $options = []) {
        $this->_isCreated(true);

        $request_options = self::normalizeRequestOptions($request_options);
        $request_options["method"] = "delete";
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]") to retrieve the user's base data
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function getUser($request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "method" => "get",
            "path" => "/"
        ], $request_options);
        return $this->request($request_options, $options);
    }

    /**
     * Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]") to update a user's base data
     *
     * @param array $data Properties to be set to the user
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
     function updateUser($data, $request_options = [], $options = []) {
        $this->_isCreated(true);

        if (empty($data)) {
            throw Exception("The data parameter must not be empty");
        }

        $request_options = array_merge([
            "method" => "put",
            "path" => "/",
            "data" => $data,
        ], $request_options);

        return $this->request($request_options, $options);
    }

    /**
     * Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/meta") to retrieve a user's meta object
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     *
     * @example `$user->getMeta()` could return something like `array("someKey" => array("anotherKey" => "someVal"))`
     * @example `$user->getMeta("someKey.anotherKey")` would return just `"someVal"`
     */
    function getMeta($request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "method" => "get",
            "path" => "/meta",
        ], $request_options);

        return $this->request($request_options, $options);
    }

    /**
     * Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]/meta") to update a user's meta object
     *
     * @param array $data Properties to be set to the meta object
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
     function updateMeta($data = [], $request_options = [], $options = []) {
        $this->_isCreated(true);

        if (empty($data)) {
            throw Exception("The data parameter must not be empty");
        }

        $request_options = array_merge([
            "method" => "put",
            "path" => "/meta",
            "data" => $data,
        ], $request_options);

        return $this->request($request_options, $options);
    }


    /**
     * Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]") to delete the user
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function deleteUser($request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "path" => "/",
            "headers" => [],
        ], $request_options);

        if (!Modifiers\has_header("Accept", $request_options)) {
            $request_options["headers"]["Accept"] = "text/html";
        }

        return $this->delete($request_options, $options);
    }

    static $image_errors = [
        "UNKNOWN"        => "The upload failed for an unknown reason",
        "NO_FACE"        => "We were unable to find a face in the image.",
        "MULTIPLE_FACES" => "More than one face was found in the image. Please take a picture where you are the only subject!",
        "LOW_RESOLUTION" => "Image is too small.",
        "BAD_FOCUS"      => "The image may be too blurry to process. Please try using the auto focus feature of your camera.",
        "BAD_CONTRAST"   => "The image may be too dark or too bright to process. Please try again in an area with better lighting.",
        "OPEN_MOUTH"     => "You might be smiling or your mouth may be open. Please try again with a neutral expression and a closed mouth.",
        "CLOSED_EYE"     => "One or both of your eyes may be closed. Please try again.",
        "GLASSES"        => "You might be wearing glasses. Please try again after removing your glasses.",
        "BAD_POSE"       => "You may not be looking directly at the camera. Please try again facing directly into the camera.",
        "CELEBRITY"      => "Are you really %s? Make sure you are uploading a selfie of YOU!",
    ];

    static function getImageErrorLanguage($key, $data = []) {
        $key = strtoupper($key);
        if ($key == "CELEBRITY") {
            $names = [];
            for ($i = 0; !empty($data["details"]["celebrities"][$i]["name"]); $i++) {
                $names[] = $data["details"]["celebrities"][$i]["name"];
            }
            if (!count($names)) {
                $names[] = "a celebrity";
            }
            return sprintf(self::$image_errors[$key], implode(", ", $names));
        }
        return self::$image_errors[$key];
    }

    /**
     * Creates a formatted Array of errors from an image upload response array
     *
     * @param array $data Assoc array of image upload response data
     * @param boolean $language If `false`, the error keys will be returned. If `true` is returned, the language for each error key will be returned (see `$image_errors` array above)
     *
     * @return array Array of formatted errors
     */
    static function getImageErrors($data, $language = false) {
        $errors = [];

        if (is_array($data) && isset($data["details"]) && is_array($data["details"])) {
            $details = $data["details"];
            if (!isset($details["face"])) {
                $key = "NO_FACE";
                if ($language) {
                    return [self::getImageErrorLanguage($key, $data)];
                }
                return [$key];
            }
            $face = $details["face"];
            if (isset($face["faces"]) && $face["faces"] > 1) {
                $key = "MULTIPLE_FACES";
                if ($language) {
                    return [self::getImageErrorLanguage($key, $data)];
                }
                return [$key];
            }
            if (empty($face["resolution"])) {
                $key = "LOW_RESOLUTION";
                if ($language) {
                    return [self::getImageErrorLanguage($key, $data)];
                }
                return [$key];
            }

            $image = [];
            if (isset($details["image"]) && is_array($details["image"])) {
                $image = $details["image"];
            }

            $face_attributes = [];
            if (!empty($details["FaceAttributes"][0])) {
                $face_attributes = $details["FaceAttributes"][0];
            }

            $image_validations = [
                "BAD_FOCUS" => empty($image["focus"]),
                "BAD_CONTRAST" => empty($face["contrast"]) || empty($image["contrast"]),
                "OPEN_MOUTH" => !empty($face["openmouth"]),
                "CLOSED_EYE" => !empty($face["closedeyes"]["left"]) || !empty($face["closedeyes"]["right"]),
                "GLASSES" => !empty($face_attributes["Glasses"]),
                "BAD_POSE" => !empty($face["pose"]["yaw"]) && $face["pose"]["yaw"] > 45,
                "CELEBRITY" => !empty($details["celebrities"])
            ];

            foreach ($image_validations as $key => $failed) {
                if ($failed) {
                    if ($language) {
                        $errors[] = self::getImageErrorLanguage($key, $data);
                    } else {
                        $errors[] = $key;
                    }
                }
            }
        }

        if (!count($errors)) {
            $key = "UNKNOWN";
            if ($language) {
                return [self::getImageErrorLanguage($key, $data)];
            }
            return [$key];
        }

        return $errors;
    }

    function normalizeImageIDArgs($image_id = null, $image_key = "default") {
        if (!is_string($image_id) && is_string($image_key) && is_string($this->props["images"][$image_key])) {
            return $this->props["images"][$image_key];
        }
        return $image_id;
    }

    /**
     * Makes a request ("POST /o/[orgid]/a/[appid]/u/[userid]/image") to upload an image to the user
     *
     * @param string|resource $file Absolute path to the image file, or a file handler
     * @param string $image_contents Raw image data
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws Exception if the file or image contents can't be obtained
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\User\ImageValidationFailed if the image does not pass chronos validation
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function uploadImage($file, $image_contents = null, $image_key = "default", $request_options = [], $options = [], $delay = 1) {
        $this->_isCreated(true);

        if (!$image_contents) {
            $fh = null;
            if (is_string($file)) {
                try {
                    $fh = fopen($file, "r");
                } catch (Exception $e) {
                    throw Exception("File does not exist or could not be read");
                }
            } else if (is_resource($file)) {
                $fh = $file;
            }
            if ($fh) {
                $image_contents = fread($fh, fstat($fh)["size"]);
            }
        }

        if (!$image_contents) {
            throw Exception("Could not obtain image contents");
        }

        $request_options = array_merge([
            "method" => "post",
            "path" => "/images",
            "data" => $image_contents,
            "params" => [],
        ], $request_options);

        if (!isset($request_options["params"]["compact"])) {
            $request_options["params"]["compact"] = false;
        }

        Modifiers\set_header("Content-Type", "image/jpeg", $request_options);

        try {
            $response = $this->request($request_options, $options);

            if (is_string($image_key) && !empty($response["json"]["id"])) {
                if (!isset($this->props["images"])) {
                    $this->props["images"] = [];
                }
                $this->props["images"][$image_key] = $response["json"]["id"];
            }

            if (is_int($delay) && $delay > 0) {
                usleep($delay * 1000 * 1000);
            }

            return $response;
        } catch (Exceptions\RequestException $e) {
            $response = $e->response;
            if (!empty($response["json"])) {
                $data = $response["json"];

                throw new Exceptions\User\ImageValidationFailed(join(", ", self::getImageErrors($data)), $response, null, $e);
            }
            throw $e;
        }
    }

    /**
     * Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]/image") to delete the user's images
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function deleteImages($request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "path" => "/image",
            "headers" => [],
        ], $request_options);

        if (!Modifiers\has_header("Accept", $request_options)) {
            $request_options["headers"]["Accept"] = "text/html";
        }

        return $this->delete($request_options, $options);
    }

    /**
     * Retrieve the raw data of a user's image
     *
     * @param string $path The path of the image, relative to the user's image path ("/o/[orgid]/a/[appid]/u/[userid]/image"). Defaults to "/"
     * @param int $width Width of the image
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function getRawImage($path = "/", $image_id = null, $image_key = "default", $width = null, $request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge_recursive([
            "path" => "/images",
            "method" => "get",
            "params" => [
                "compact" => null
            ]
        ], $request_options);

        $image_id = $this->normalizeImageIDArgs($image_id, $image_key);
        if (is_string($image_id)) {
            $request_options["path"] = "/images/$image_id";
        }

        if ($path) {
            $request_options["path"] = trim($request_options["path"], "/") . "/" . trim($path, "/");
        }

        Modifiers\set_header("Accept", "image/jpeg", $request_options);

        return $this->request($request_options, $options);
    }

    /**
     * Output a user's image to the browser, and exit the script
     *
     * @param string $path The path of the image, relative to the user's image path ("/o/[orgid]/a/[appid]/u/[userid]/image")
     * @param null|int $width Width of the image
     * @param string $filename The name of the outputted file
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     */
    function outputImage($path = "/", $width = null, $filename = "image.jpg", $request_options = [], $options = []) {
        $this->_isCreated(true);

        $res = $this->getRawImage($path, $width, $request_options, $options)["text"];
        header("Content-Type: image/jpg");
        header('Content-Disposition: inline; filename="' . $filename . '";');
        echo $res;
        exit;
    }

    /**
     * Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/image/face/estimations") for the estimations on the user's image
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function getEstimations($image_id = null, $image_key = "default", $request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "path" => "/image/face/estimations",
        ], $request_options);

        $image_id = $this->normalizeImageIDArgs($image_id, $image_key);
        if (is_string($image_id)) {
            $request_options["path"] = "/images/$image_id/face/estimations";
        }

        return $this->get($request_options, $options);
    }

    /**
     * Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/lifespan") to get the user's lifespan
     *
     * @param array $request_options Request options array (see `Chronos::request`)
     * @param array $options Options to be passed to the request
     *
     * @throws LapetusSolutions\Chronos\Exceptions\User\UserNotCreated if the user is not created
     * @throws LapetusSolutions\Chronos\Exceptions\RequestException if there is an error while making the request
     *
     * @return array Response from the request (see `Chronos::request`)
     */
    function getLifespan($request_options = [], $options = []) {
        $this->_isCreated(true);

        $request_options = array_merge([
            "path" => "/lifespan",
            "params" => [],
        ], $request_options);

        if (!isset($request_options["params"]["compact"])) {
            $request_options["params"]["compact"] = false;
        }

        return $this->get($request_options, $options);
    }
}
