<?php

namespace LapetusSolutions\Chronos\Modifiers\User;
use \LapetusSolutions\Chronos;

class UrlModifier extends Chronos\RequestModifier {
    /**
     * Request modifier to add the user's base path to the beginning of the path
     */

    /**
     * Transforms the path to /u/[userid]/[previous path]
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        $request_options["path"] = "/u/" . $this->options["instance"]->props["id"] . "/" . trim($request_options["path"], "/");
    }
}
