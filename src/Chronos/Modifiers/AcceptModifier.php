<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

require_once("helpers.php");

/**
 * Request modifier to set the `Accept` header
 */
class AcceptModifier extends Chronos\RequestModifier {
    /**
     * Sets the `Accept` header to "application/json" if the `Accept` header is not already set
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        set_header("Accept", "application/json", $request_options);
    }
}
