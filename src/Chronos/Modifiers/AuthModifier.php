<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

/**
 * Request modifier wrapper for applying the `auth` property request modifier of the instance
 */
class AuthModifier extends Chronos\RequestModifier {
    /**
     * (array) Assoc array of authentication modifiers
     */
    static $methods = [
        "default" => "\LapetusSolutions\Chronos\Modifiers\Auth\AuthDefaultModifier"
    ];

    /**
     * Applies the $instance's `auth` modifier
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        Chronos\Chronos::applyRequestModifier($this->options["instance"]->auth, $this->options["instance"], $request_options, $options);
    }
}
