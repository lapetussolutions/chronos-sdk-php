<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

require_once("helpers.php");

/**
 * Request modifier to set the `Content-Type` header
 */
class ContentTypeModifier extends Chronos\RequestModifier {
    /**
     * Sets the `Content-Type` header to "application/json" if the `Content-Type` header is not already set
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        set_header("Content-Type", "application/json", $request_options);
    }
}
