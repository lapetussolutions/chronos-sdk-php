<?php

namespace LapetusSolutions\Chronos\Modifiers;

function normalize_header_keys($request_options) {
    if (!isset($request_options["headers"])) {
        return [];
    }
    $res = [];
    foreach ($request_options["headers"] as $key => $value) {
        $res[strtolower($key)] = $value;
    }
    return $res;
}


/**
 * Helper function to check whether a request options array contains a header
 *
 * @param string $header Header name
 * @param array $request_options Request options
 */
function has_header($header, $request_options) {
    $headers = normalize_header_keys($request_options);
    return isset($headers[$header]);
}

/**
 * Helper function to get a header from the request options
 *
 * @param string $header Header name
 * @param array $request_options Request options
 */
function get_header($header, $request_options, $default = null) {
    $headers = normalize_header_keys($request_options);
    return isset($headers[$header]) ? $headers[$header] : $default;
}

/**
 * Helper function to set a header from the request options if it isn't already set
 *
 * @param string $header Header name
 * @param array $request_options Request options
 */
function set_header($header, $value, &$request_options, $force = false) {
    $headers = normalize_header_keys($request_options);
    if (isset($headers[strtolower($header)]) && !$force) {
        return;
    }
    if (!isset($request_options["headers"])) {
        $request_options["headers"] = [];
    }
    return $request_options["headers"][$header] = $value;
}
