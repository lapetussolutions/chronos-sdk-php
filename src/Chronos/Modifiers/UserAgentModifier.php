<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

require_once("helpers.php");

/**
 * Request modifier to set the `User-Agent` header
 */
class UserAgentModifier extends Chronos\RequestModifier {
    /**
     * Sets the `User-Agent` header to the instance's `config["user_agent"]` property if the `User-Agent` header is not already set
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        set_header("User-Agent", $instance->config["user_agent"], $request_options);
    }
}
