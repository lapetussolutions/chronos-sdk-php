<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

/**
 * Request modifier to set the URL of the request
 */
class UrlModifier extends Chronos\RequestModifier {
    /**
     * Removes leading and trailing forward slashes
     * @param string $url
     */
    static function stripUrl($url) {
        return trim($url, "/");
    }
    /**
     * Joins multiple urls together with a single forward slash between each url
     * @param array $urls
     */
    static function urlJoin($urls) {
        foreach ($urls as &$url) {
            $url = self::stripUrl($url);
        }
        return join("/", $urls);
    }

    /**
     * Sets the URL of the request to "[apiroot]/[url]/[path]"
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        if (!isset($request_options["url"])) {
            $request_options["url"] = self::urlJoin([$instance->props["apiroot"], $instance->appHref()]);
        }
        if (empty($request_options["path"]) || !is_string($request_options["path"])) {
            return;
        }
        $request_options["url"] = self::urlJoin([$request_options["url"], $request_options["path"]]);
    }
}
