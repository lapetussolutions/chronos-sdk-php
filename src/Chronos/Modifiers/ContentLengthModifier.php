<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

require_once("helpers.php");

/**
 * Request modifier to set the `Content-Length` header
 */
class ContentLengthModifier extends Chronos\RequestModifier {
    /**
     * Sets the `Content-Length` header to the string length of the json encoded data if the `Content-Length` header is not already set
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        if (!isset($request_options["data"]) || has_header("Content-Length", $request_options)) {
            return;
        }

        $data = $request_options["data"];
        if (!is_string($data)) {
            $data = json_encode($data);
        }

        $request_options["headers"]["Content-Length"] = strlen($data);
    }
}
