<?php

namespace LapetusSolutions\Chronos\Modifiers\Auth;
use \LapetusSolutions\Chronos;

/**
 * Request modifier for setting the "Authorization" header in the following format: "[authorization_type] [token]"
 */
class AuthDefaultModifier extends Chronos\RequestModifier {
    /**
     * @param $options Request modifier options
     *
     * $options
     *
     * ['props_key'] (`string`) - Name of the instance's props array property,
     * ['token_key'] (`string`) - Key in which the token is stored in the instance's prop array,
     * ['authorization_type'] (`string`) - Prefix of the `Authorization` header value. Defaults to the value of 'token_key'
     *
     */
    public function __construct($options = []) {
        parent::__construct($options);
        if (!isset($this->options["props_key"])) {
            $this->options["props_key"] = "props";
        }
        if (!isset($this->options["token_key"])) {
            $this->options["token_key"] = "apikey";
        }
        if (!isset($this->options["authorization_type"])) {
            $this->options["authorization_type"] = $this->options["token_key"];
        }
    }

    /**
     * Sets the Authorization header. The options define which properties are pulled from the instance
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        if (!isset($request_options["headers"])) {
            $request_options["headers"] = [];
        }

        $props = $instance->{$this->options["props_key"]};
        $token = $props[$this->options["token_key"]];
        $request_options["headers"]["Authorization"] =  $this->options["authorization_type"] . " " . $token;
    }
}
