<?php

namespace LapetusSolutions\Chronos\Modifiers;
use \LapetusSolutions\Chronos;

/**
 * Request modifier to set the `compact` query string parameter
 */
class CompactModifier extends Chronos\RequestModifier {
    /**
     * Adds the `compact` query string parameter if the instance's `config["compact_mode"]` is `true`
     *
     * @param object $instance Object to be used by the modifier
     * @param array $request_options Request options array to be modified
     * @param array $options Options to be used by the modifier
     */
    public function requestOptions($instance, &$request_options, $options = []) {
        parent::requestOptions($instance, $request_options, $options);

        if (empty($instance->config["compact_mode"]) || isset($request_options["params"]["compact"])) {
            return;
        }

        if (!isset($request_options["params"])) {
            $request_options["params"] = [];
        }
        $request_options["params"]["compact"] = "true";
    }
}
