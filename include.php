<?php

/* Include Chronos class files if not using composer */
require(__DIR__ . "/src/Chronos/Chronos.php");
require(__DIR__ . "/src/Chronos/User.php");
require(__DIR__ . "/src/Chronos/RequestModifier.php");

require(__DIR__ . "/src/Chronos/Exceptions/RequestException.php");
require(__DIR__ . "/src/Chronos/Exceptions/InvalidResponseFormat.php");
require(__DIR__ . "/src/Chronos/Exceptions/User/UserAlreadyCreated.php");
require(__DIR__ . "/src/Chronos/Exceptions/User/UserNotCreated.php");
require(__DIR__ . "/src/Chronos/Exceptions/User/ImageValidationFailed.php");

require(__DIR__ . "/src/Chronos/Modifiers/Auth/AuthDefaultModifier.php");

require(__DIR__ . "/src/Chronos/Modifiers/User/UrlModifier.php");

require(__DIR__ . "/src/Chronos/Modifiers/AuthModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/CompactModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/ContentLengthModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/ContentTypeModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/AcceptModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/UrlModifier.php");
require(__DIR__ . "/src/Chronos/Modifiers/UserAgentModifier.php");
