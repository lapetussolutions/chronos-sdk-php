sed -i '' '187 c\
$string .= $this->indent($description . "\\n\\n", 4, false); \
' ./vendor/sphpdox/sphpdox/lib/Sphpdox/Element/MethodElement.php
sed -i '' '34 c\
$string .= $this->indent($parser->getDescription(), 4, false); \
' ./vendor/sphpdox/sphpdox/lib/Sphpdox/Element/PropertyElement.php
sed -i '' '47 c\
$line = $this->indent($line, 4, false); \
' ./vendor/sphpdox/sphpdox/lib/Sphpdox/Element/PropertyElement.php

rm -rf source/LapetusSolutions
./vendor/bin/sphpdox process "LapetusSolutions\\Chronos" ../src/ "$@"