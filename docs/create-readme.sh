#!/bin/sh

project_dir="$(git rev-parse --show-toplevel)"
cd "$project_dir"

echo "# Chronos SDK - PHP

An SDK for interacting with Chronos API v1.0.0 in PHP versions 5.3 and above
" > README.md

cat docs/source/installation.md >> README.md
echo "" >> README.md
cat docs/source/examples.md >> README.md
