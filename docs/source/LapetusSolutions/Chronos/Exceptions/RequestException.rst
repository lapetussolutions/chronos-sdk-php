-------------------------------------------------------
LapetusSolutions\\Chronos\\Exceptions\\RequestException
-------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Exceptions

.. php:class:: RequestException

    .. php:attr:: response

        array

    .. php:attr:: message

        protected

    .. php:attr:: code

        protected

    .. php:attr:: file

        protected

    .. php:attr:: line

        protected

    .. php:method:: __construct($message, $response, $code = 0, $previous = NULL)

        :param $message:
        :param $response:
        :param $code:
        :param $previous:

    .. php:method:: __clone()

    .. php:method:: __wakeup()

    .. php:method:: getMessage()

    .. php:method:: getCode()

    .. php:method:: getFile()

    .. php:method:: getLine()

    .. php:method:: getTrace()

    .. php:method:: getPrevious()

    .. php:method:: getTraceAsString()

    .. php:method:: __toString()
