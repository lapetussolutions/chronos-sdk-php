-----------------------------------------------------------
LapetusSolutions\\Chronos\\Exceptions\\User\\UserNotCreated
-----------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Exceptions\\User

.. php:class:: UserNotCreated

    .. php:attr:: message

        protected

    .. php:attr:: code

        protected

    .. php:attr:: file

        protected

    .. php:attr:: line

        protected

    .. php:method:: __construct($message, $code = 0, Exception $previous = NULL)

        Exception used for indicating a user has not been created

        :param $message:
        :param $code:
        :type $previous: Exception
        :param $previous:

    .. php:method:: __clone()

    .. php:method:: __wakeup()

    .. php:method:: getMessage()

    .. php:method:: getCode()

    .. php:method:: getFile()

    .. php:method:: getLine()

    .. php:method:: getTrace()

    .. php:method:: getPrevious()

    .. php:method:: getTraceAsString()

    .. php:method:: __toString()
