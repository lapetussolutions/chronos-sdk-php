------------------------------------------------------------------
LapetusSolutions\\Chronos\\Exceptions\\User\\ImageValidationFailed
------------------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Exceptions\\User

.. php:class:: ImageValidationFailed

    .. php:attr:: response

        array

    .. php:attr:: message

        protected

    .. php:attr:: code

        protected

    .. php:attr:: file

        protected

    .. php:attr:: line

        protected

    .. php:method:: __construct($message, $response, $code = 0, $previous = NULL)

        Exception used for indicating a user has not been created

        :param $message:
        :param $response:
        :param $code:
        :param $previous:

    .. php:method:: __clone()

    .. php:method:: __wakeup()

    .. php:method:: getMessage()

    .. php:method:: getCode()

    .. php:method:: getFile()

    .. php:method:: getLine()

    .. php:method:: getTrace()

    .. php:method:: getPrevious()

    .. php:method:: getTraceAsString()

    .. php:method:: __toString()
