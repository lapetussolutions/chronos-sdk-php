-----------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\CompactModifier
-----------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers

.. php:class:: CompactModifier

    Request modifier to set the `compact` query string parameter

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Adds the `compact` query string parameter if the instance's `config["compact_mode"]` is `true`

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
