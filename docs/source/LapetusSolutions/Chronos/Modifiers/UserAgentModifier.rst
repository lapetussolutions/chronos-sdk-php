-------------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\UserAgentModifier
-------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers

.. php:class:: UserAgentModifier

    Request modifier to set the `User-Agent` header

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Sets the `User-Agent` header to the instance's `config["user_agent"]` property if the `User-Agent` header is not already set

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
