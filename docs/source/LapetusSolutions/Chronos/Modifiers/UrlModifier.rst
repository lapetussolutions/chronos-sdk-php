-------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\UrlModifier
-------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers

.. php:class:: UrlModifier

    Request modifier to set the URL of the request

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: stripUrl($url)

        Removes leading and trailing forward slashes

        :type $url: string
        :param $url:

    .. php:method:: urlJoin($urls)

        Joins multiple urls together with a single forward slash between each url

        :type $urls: array
        :param $urls:

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Sets the URL of the request to "[apiroot]/[url]/[path]"

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
