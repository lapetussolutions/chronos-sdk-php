----------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\AcceptModifier
----------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers

.. php:class:: AcceptModifier

    Request modifier to set the `Accept` header

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Sets the `Accept` header to "application/json" if the `Accept` header is not already set

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
