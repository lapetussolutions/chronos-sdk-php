--------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\AuthModifier
--------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers

.. php:class:: AuthModifier

    Request modifier wrapper for applying the `auth` property request modifier of the instance

    .. php:attr:: methods

        (array) Assoc array of authentication modifiers

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Applies the $instance's `auth` modifier

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
