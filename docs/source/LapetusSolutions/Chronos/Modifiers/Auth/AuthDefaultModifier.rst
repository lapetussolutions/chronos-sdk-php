---------------------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\Auth\\AuthDefaultModifier
---------------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers\\Auth

.. php:class:: AuthDefaultModifier

    Request modifier for setting the "Authorization" header in the following format: "[authorization_type] [token]"

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: __construct($options = [])

        $options

        ['props_key'] (`string`) - Name of the instance's props array property,
        ['token_key'] (`string`) - Key in which the token is stored in the instance's prop array,
        ['authorization_type'] (`string`) - Prefix of the `Authorization` header value. Defaults to the value of 'token_key'

        :param $options:

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Sets the Authorization header. The options define which properties are pulled from the instance

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier
