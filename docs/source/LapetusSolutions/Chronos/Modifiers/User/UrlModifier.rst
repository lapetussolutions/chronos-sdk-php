-------------------------------------------------------
LapetusSolutions\\Chronos\\Modifiers\\User\\UrlModifier
-------------------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos\\Modifiers\\User

.. php:class:: UrlModifier

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Transforms the path to /u/[userid]/[previous path]

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options
