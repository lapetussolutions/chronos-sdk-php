-------------------------------
LapetusSolutions\\Chronos\\User
-------------------------------

.. php:namespace: LapetusSolutions\\Chronos

.. php:class:: User

    Class for interacting with users within the Chronos API

    .. php:attr:: _chronos

        LapetusSolutions\\Chronos\\Chronos

    .. php:attr:: props

        array

        $props

        - ['id'] (`string`) - User ID
        - ['auth_token'] (`string`) - User API auth token

    .. php:attr:: auth

        LapetusSolutions\\Chronos\\RequestModifier|function

    .. php:attr:: modifiers

        array

    .. php:attr:: image_errors

    .. php:method:: __construct($chronos, $props = [])

        Class constructor

        :type $chronos: LapetusSolutions\Chronos\Chronos
        :param $chronos: Chronos instance
        :type $props: array
        :param $props: User properties (see `$props` above). If you want to access and/or manipulate a user already created in Chronos, pass the user's `id` and `auth_token` in this param
        :returns: void

    .. php:method:: isCreated()

        Returns a boolean of whether the user has the `id` property set

        :returns: boolean

    .. php:method:: _isCreated($should_be_created = true)

        Throws an exception if the user's created state (`isCreated()`) does not match the `$should_be_created` argument

        :type $should_be_created: boolean
        :param $should_be_created: Whether or not the user should be created. Defaults to `true`

    .. php:method:: setAuthMethod($method = "default", $options = [])

        Sets the authentication request modifier

        :type $method: LapetusSolutions\Chronos\RequestModifier|function|string
        :param $method: Authentication request modifier. Defaults to "default"
        :type $options: array
        :param $options: Options to be passed to the modifier. Only used if `$method` is a `LapetusSolutions\Chronos\RequestModifier` or `string`

    .. php:method:: normalizeRequestOptions($request_options)

        Makes the request options an array if a string is passed (see `Chronos::normalizeRequestOptions`)

        :type $request_options: array
        :param $request_options: Request options array
        :returns: array

    .. php:method:: create($data = [], $request_options = [], $options = [])

        Makes a request ("POST /o/[orgid]/a/[appid]/users") to create a user in Chronos

        :type $data: array
        :param $data: User data to be sent in the request
        :param $request_options:
        :param $options:
        :returns: array Request response (see `Chronos::request`)

    .. php:method:: requestOptions($request_options, $options = [])

        :param $request_options:
        :param $options:

    .. php:method:: request($request_options, $options = [])

        :param $request_options:
        :param $options:

    .. php:method:: get($request_options, $options = [])

        Makes a request using the GET method

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: post($request_options, $options = [])

        Makes a request using the POST method

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: put($request_options, $options = [])

        Makes a request using the PUT method

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: delete($request_options, $options = [])

        Makes a request using the DELETE method

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getUser($request_options = [], $options = [])

        Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]") to retrieve the user's base data

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: updateUser($data, $request_options = [], $options = [])

        Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]") to update a user's base data

        :type $data: array
        :param $data: Properties to be set to the user
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getMeta($request_options = [], $options = [])

        Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/meta") to retrieve a user's meta object

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: updateMeta($data = [], $request_options = [], $options = [])

        Makes a request ("PUT /o/[orgid]/a/[appid]/u/[userid]/meta") to update a user's meta object

        :type $data: array
        :param $data: Properties to be set to the meta object
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: deleteUser($request_options = [], $options = [])

        Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]") to delete the user

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getImageErrorLanguage($key, $data = [])

        :param $key:
        :param $data:

    .. php:method:: getImageErrors($data, $language = false)

        Creates a formatted Array of errors from an image upload response array

        :type $data: array
        :param $data: Assoc array of image upload response data
        :type $language: boolean
        :param $language: If `false`, the error keys will be returned. If `true` is returned, the language for each error key will be returned (see `$image_errors` array above)
        :returns: array Array of formatted errors

    .. php:method:: uploadImage($file, $image_contents = null, $request_options = [], $options = [])

        Makes a request ("POST /o/[orgid]/a/[appid]/u/[userid]/image") to upload an image to the user

        :type $file: string|resource
        :param $file: Absolute path to the image file, or a file handler
        :type $image_contents: string
        :param $image_contents: Raw image data
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: deleteImages($request_options = [], $options = [])

        Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]/image") to delete the user's images

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getRawImage($path = "/", $width = null, $request_options = [], $options = [])

        Retrieve the raw data of a user's image

        :type $path: string
        :param $path: The path of the image, relative to the user's image path ("/o/[orgid]/a/[appid]/u/[userid]/image"). Defaults to "/"
        :type $width: int
        :param $width: Width of the image
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: outputImage($path = "/", $width = null, $filename = "image.jpg", $request_options = [], $options = [])

        Output a user's image to the browser, and exit the script

        :type $path: string
        :param $path: The path of the image, relative to the user's image path ("/o/[orgid]/a/[appid]/u/[userid]/image")
        :type $width: null|int
        :param $width: Width of the image
        :type $filename: string
        :param $filename: The name of the outputted file
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request

    .. php:method:: getEstimations($request_options = [], $options = [])

        Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/image/face/estimations") for the estimations on the user's image

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getLifespan($request_options = [], $options = [])

        Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/lifespan") to get the user's lifespan

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: uploadIDImage($file, $image_contents = null, $side = "front", $request_options = [], $options = [])

        Makes a request ("POST /o/[orgid]/a/[appid]/u/[userid]/images/id/[side]") to upload an ID image to the user

        :type $file: string|resource
        :param $file: Absolute path to the image file, or a file handler
        :type $image_contents: string
        :param $image_contents: Raw image data
        :type $side: string
        :param $side: Side of ID. Defaults to "front"
        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: getIDImageAnalysis($request_options = [], $options = [])

        Makes a request ("GET /o/[orgid]/a/[appid]/u/[userid]/images/id") to get the ID analysis

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)

    .. php:method:: deleteIDImages($request_options = [], $options = [])

        Makes a request ("DELETE /o/[orgid]/a/[appid]/u/[userid]/images/id") to delete the ID images and analysis

        :type $request_options: array
        :param $request_options: Request options array (see `Chronos::request`)
        :type $options: array
        :param $options: Options to be passed to the request
        :returns: array Response from the request (see `Chronos::request`)
