------------------------------------------
LapetusSolutions\\Chronos\\RequestModifier
------------------------------------------

.. php:namespace: LapetusSolutions\\Chronos

.. php:class:: RequestModifier

    Base class for making custom request modifiers

    .. php:attr:: options

        (`array`) Request modifier options

    .. php:method:: __construct($options = [])

        :type $options: array
        :param $options: Request modifier options

    .. php:method:: requestOptions($instance, $request_options, $options = [])

        Method to be overwritten to modify requests

        :type $instance: object
        :param $instance: Object to be used by the modifier
        :type $request_options: array
        :param $request_options: Request options array to be modified
        :type $options: array
        :param $options: Options to be used by the modifier
