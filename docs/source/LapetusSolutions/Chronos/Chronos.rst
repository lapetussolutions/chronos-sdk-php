----------------------------------
LapetusSolutions\\Chronos\\Chronos
----------------------------------

.. php:namespace: LapetusSolutions\\Chronos

.. php:class:: Chronos

    Class for interacting with the Chronos API

    .. php:attr:: version

        (`string`) Chronos API Version

    .. php:attr:: props

        (`array`) Properties array containing Chronos API endpoint and credentials:

        - ['apiroot'] (`string`) - Chronos API root
        - ['orgid'] (`string`) - Chronos Organization ID
        - ['apikey'] (`string`) - Chronos Organization API Key
        - ['appid'] (`string`) - Chronos App ID

    .. php:attr:: config

        (`array`) Configuration array

        - ['compact_mode'] (`boolean`) - If `true`, all requests will have the "compact=true" query string parameter added. Defaults to `true`
        - ['user_agent']   (`string`) - User agent to be sent in each request. Defaults to `"chronos-sdk-php/1.0.0 (PHP [PHP version]; cURL [cURL version])"`

    .. php:attr:: auth

        LapetusSolutions\\Chronos\\RequestModifier|function|string

    .. php:attr:: modifiers

        (`array`) Array of default request modifiers

        - ['pre']  (`array`) - Array of request modifiers to be applied to all requests
        - ['post'] (`array`) - Array of request modifiers to be applied to all requests after the 'pre' list

    .. php:method:: __construct($props, $config = [])

        Class constructor

        :type $props: array
        :param $props: Properties array (see above)
        :type $config: array
        :param $config: Configuration array (see above)

    .. php:method:: setAuthMethod($method = "default", $options = [])

        Sets the authentication request modifier

        :type $method: LapetusSolutions\\Chronos\\RequestModifier|function|string
        :param $method: Authentication request modifier. Defaults to `"default"`
        :type $options: array
        :param $options: Options to be passed to the modifier. Only used if `$method` is a `LapetusSolutions\\Chronos\\RequestModifier` or `string`

    .. php:method:: applyRequestModifier($modifier, $instance, $request_options, $options = [])

        Applies a modifier to a request options array

        :type $modifier: LapetusSolutions\\Chronos\\RequestModifier|function|string
        :param $modifier: Authentication request modifier
        :type $instance: LapetusSolutions\\Chronos\\Chronos
        :param $instance: Instance to pass to the modifier
        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifier

    .. php:method:: applyRequestModifiers($modifiers, $instance, $request_options, $options = [])

        Applies an array of modifiers to a request options array

        :type $modifiers: (LapetusSolutions\\Chronos\\RequestModifier|function)[]
        :param $modifiers: Authentication request modifiers
        :type $instance: LapetusSolutions\\Chronos\\Chronos
        :param $instance: Instance to pass to the modifiers
        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers

    .. php:method:: combineModifiers($modifier_objects, $order = [ "pre", "post" ])

        Combines an array of modifier objects into an array of modifiers

        :type $modifier_objects: array
        :param $modifier_objects: Array of modifier objects
        :type $order: array
        :param $order: Order of modifier array keys. Defaults to [ "pre", "post" ]
        :returns: array Array of modifiers

    .. php:method:: orgHref()

        :returns: string Path of the Chronos orginization

    .. php:method:: appHref()

        :returns: string Path of the Chronos app

    .. php:method:: url($instance, $request_options, $options = [])

        Request modifier to create the base url for the orgid and appid in the `$props` property

        $request_options

        - ['url']  (`string`) - Base url for a request. Defaults to the instance's `appHref()`
        - ['path'] (`string`) - Path to be added to the base url

        :type $instance: LapetusSolutions\\Chronos\\Chronos
        :param $instance: Chronos instance. This parameter is ignored
        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers

    .. php:method:: requestOptions($request_options, $options = [])

        Creates full request options array

        $request_options

        - ['url']  (`string`) - Request base url. Defaults to the instance's `appHref()`
        - ['path'] (`string`) - Path to be added to the base url

        $options

        - ['modifiers'] (`array`) - Modifiers to be combined with the instance's default modifiers and applied to the request options
        - ['override_modifiers'] (`boolean`) - If `true`, the instance's default modifiers will not be applied to the request options

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers

    .. php:method:: normalizeRequestOptions($request_options)

        Makes the request options a array if a string is passed

        :type $request_options: array
        :param $request_options: Request options array
        :returns: array

    .. php:method:: request($request_options, $options = [])

        Makes a request

        $request_options

        - ['method'] (`string`) - Request method. Defaults to "get"
        - ['url'] (`string`) - Base url for a request. Defaults to the instance's `appHref()`
        - ['path'] (`string`) - Path to be added to the base url
        - ['headers'] (`array`) - Request headers
        - ['params'] (`array`) - Request query string
        - ['data'] (`mixed`) - Request data

        $options

        - ['modifiers'] (`array`) - Modifiers to be combined with the instance's default modifiers and applied to the request options
        - ['override_modifiers'] (`boolean`) - If `true`, the instance's default modifiers will not be applied to the request options

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers
        :returns: array Assoc array with the following keys: "text" (raw response text), "json" (assoc array of the json decoded response text), "headers" (assoc array of the response headers), and "status" (status code)

    .. php:method:: get($request_options, $options = [])

        Makes a request using the GET method. See `request` method for more details

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers
        :returns: array Response from the request (see `request`)

    .. php:method:: post($request_options, $options = [])

        Makes a request using the POST method. See `request` method for more details

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers
        :returns: array Response from the request (see `request`)

    .. php:method:: put($request_options, $options = [])

        Makes a request using the PUT method. See `request` method for more details

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers
        :returns: array Response from the request (see `request`)

    .. php:method:: delete($request_options, $options = [])

        Makes a request using the DELETE method. See `request` method for more details

        :type $request_options: array
        :param $request_options: Request options array
        :type $options: array
        :param $options: Options to be passed to the modifiers
        :returns: array Response from the request (see `request`)

    .. php:method:: user($props = [])

        Create an instance of the User class.
        If you want to access and/or manipulate a user already created in Chronos, pass the user's `id` and `auth_token` in the $props param

        :type $props: array|object
        :param $props: Properties of the Chronos user
        :returns: LapetusSolutions\\Chronos\\User Instance of the User class

    .. php:method:: createUser($data = [], $props = [], $request_options = [], $options = [])

        Creates an instance of the User class, and saves the user in Chronos.
        Do not use this if the user already exists in Chronos, see the `user` method above

        :type $data: array
        :param $data: User data to be sent in the create request
        :type $props: array
        :param $props: Properties of the Chronos user
        :param $request_options:
        :param $options:
        :returns: LapetusSolutions\\Chronos\\User Instance of the User class
