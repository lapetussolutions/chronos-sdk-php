## Examples

### Create a Chronos instance

```php
<?php

use \LapetusSolutions\Chronos;
// Classes can now be accessed from Chronos\Chronos and Chronos\User

$chronos = new Chronos\Chronos([
    "apiroot" => "https://chronos.lapetussolutions.com/v1",
    "orgid" => "[orgid]",
    "appid" => "[appid]",
    "apikey" => "[apikey]",
]);
```

### Chronos Methods

#### Make a Request


```php
<?php

$request_options = [
    # http method
    "method" => "get",

    # post data
    "data" => [],

    # query string parameters
    "params" => [],

    # headers
    "headers" => [],
];

$res = $chronos->request($request_options);

```

The return value for any request method will be an array in the following format:

- ['text'] (string) - Response text contents
- ['json'] (array) - Response text contents, parsed as JSON
- ['status'] (int) - Response status code

### User Methods

#### Create a User

```php
<?php

$props = [
    "dob" => "YYYYMMDD",
    "gender" => "[male|female]",
    "email" => "some-email@domain.com",
    "name" => "John Doe",
    "postcode" => "12345",
    "loc" => "USA",
    "meta" => [
        "some_key" => "some_val",
    ],
];

$user = $chronos->createUser($props);
echo "created: ". $user->isCreated();
```

#### Access an Existing User

```php
<?php

$user = $chronos->user([
    "id" => "[user id]",
    "auth_token" => "[user auth token]",
]);
```

#### User Data Object

##### Get

```php
<?php

$response = $user->getUser();
print_r($response["json"]);

```

##### Update

```php
<?php

$props = [
    "dob" => "19800101",
    "meta" => [
        "some_other_key" => "some_other_val",
        "nested" => [
            "keys" => [
                "work" => "too"
            ],
        ],
        "so.do.keys.like" => "this"
    ],
];

$user->updateUser($props);

```

#### User Meta Object

The following two methods are for the meta object, a subset of the data object

##### Get

```php
<?php

$response = $user->getMeta();
print_r($response["json"]);

```

##### Update

```php
<?php

$meta = [
    "more.nested.keys" => ["more", "values"],
];

$user->updateMeta($meta);
```

#### Upload an Image

Note: This will overwrite whatever image is currently uploaded to the user, regardless of whether the request results in an error.

```php
<?php

$filename = "/path/to/image.jpg";
$user->uploadImage($filename);

```

Invalid image upload errors:

```php
<?php

// In this example, the subject in the image has their eyes closed.
$filename = "/path/to/invalid-image.jpg";

try {
    $user->uploadImage($filename);
} catch (Chronos\Exceptions\User\ImageValidationFailed $e) {
    print_r(Chronos\User::getImageErrors($e->response["json"]));
}

```

The criteria for image quality can be found [here](../../working-with-images.html)

#### ID Images

##### Upload

You may upload 1 image per "side" of the ID (currently the "front" and "back" sides are supported). Uploading an image to a side will overwrite the image for that side.

```php
<?php

$user->uploadIDImage($front_filename, null, "front");
$user->uploadIDImage($back_filename, null, "back");

```

##### Get Analysis

After uploading at least the "front" image of the ID, you can use the `getIDImageAnalysis` method to get the analysis of the image. This call is asynchronous, so you will need to call it once, and then after 10 seconds call at whichever interval you want (1 second in this example) until the status is no longer "In Progress".

```php
<?php

$analysis = $instance->getIDImageAnalysis();
sleep(10);

for ($i = 0; $i < 30; $i++) {
    $analysis = $instance->getIDImageAnalysis()["json"];
    if ($analysis["status"] != "In Progress") {
        break;
    }
    sleep(1);
}

```

##### Delete

You can delete the user's ID images and analysis by using the `deleteIDImages` method:

```php
<?php

$user->deleteIDImages();

```


### Handling Exceptions

#### Requests

The `LapetusSolutions\Chronos\Exceptions\RequestException` exception will be thrown if there is a curl error while making the request, or if the response status is 4XX or 5XX

It is recommended that you account for exceptions for all requests, especially if it is a client-facing project

```php
<?php

use \LapetusSolutions\Chronos;

$chronos = new Chronos\Chronos([
    "apiroot" => "https://chronos.lapetussolutions.com/v1",
    "orgid" => "[orgid]",
    "appid" => "[appid]",
    "apikey" => "[apikey]",
]);

try {
    $chronos->get("/some/invalid/request");
} catch (Chronos\Exceptions\RequestException $e) {
    echo($e->getMessage());
    /* The request failed for an unknown reason. Check the response for more details */
    echo($e->response["text"]);
    /* {"status":404,"code":404,"message":"Unknown Request","developerMessage":"The requested URI was not found."} */
    print_r($e->response["json"]);
    /* Array
    (
        [status] => 404
        [code] => 404
        [message] => Unknown Request
        [developerMessage] => The requested URI was not found.
    )
    */
    echo($e->response["status"]);
    /* 404 */
}

```

#### User Exceptions

##### Image Upload Errors

```php
<?php

$user = $chronos->createUser();

try {
    $user->uploadImage("/eyes/closed/image.jpg");
} catch (Chronos\Exceptions\User\ImageValidationFailed $e) {
    echo($e->getMessage());
    /* One or both of your eyes may be closed. Please try again. */
    echo($e->response["text"]);
    /* {"status":400,"code":400,"message":"Image failed quality checks","developerMessage":"Check 'details' for the reason the image failed and try again.","details":{"id":"","image":{...},"face":{...}}} */
    print_r($e->response["json"]);
    /* Array
    (
        [status] => 400
        [code] => 400
        [message] => Image failed quality checks
        [developerMessage] => Check 'details' for the reason the image failed and try again.
        [details] => Array
            (
                [id] =>
                [image] => Array
                    (
                        ...
                    )
                [face] => Array
                    (
                        ...
                    )
            )
    )
    */
    echo($e->response["status"]);
    /* 400 */
    print_r(Chronos\User::getImageErrors($e->response["json"]));
    /* Array
    (
        [0] CLOSED_EYE
    )
    */
    print_r(Chronos\User::getImageErrors($e->response["json"]), true);
    /* Array
    (
        [0] => One or both of your eyes may be closed. Please try again
    )
    */
}

```

##### User Not Created

A user is "created" if the `id` and `auth_token` properties are set. You can check the created status by using `->isCreated()`

Any user method that makes a request (with the exception of `->create`) will throw a `LapetusSolutions\Chronos\Exceptions\User\UserNotCreated` exception if the user is not "created"

```php
<?php

# user properties not passed
$user = $chronos->user();
$user->get("/");

/*
Fatal error: Uncaught exception 'LapetusSolutions\Chronos\Exceptions\User\UserNotCreated' with message 'User must be created to use this method' in [chronos path]/src/Chronos/User.php:88
Stack trace:
#0 [chronos path]/src/Chronos/User.php(224): LapetusSolutions\Chronos\User->_isCreated(true)
#1 [example file]: LapetusSolutions\Chronos\User->get('/')
#2 {main}
  thrown in [chronos path]/src/Chronos/User.php on line 88
*/

```

##### User Already Created

If `->create` is called and the user is already "created", it will raise a `LapetusSolutions\Chronos\Exceptions\User\UserAlreadyCreated` exception

```php
<?php

$user = $chronos->createUser();
$user->create();

/*
Fatal error: Uncaught exception 'LapetusSolutions\Chronos\Exceptions\User\UserAlreadyCreated' with message 'User must not be created to use this method' in [chronos path]/src/Chronos/User.php:90
Stack trace:
#0 [chronos path]/src/Chronos/User.php(148): LapetusSolutions\Chronos\User->_isCreated(false)
#1 [example file]: LapetusSolutions\Chronos\User->create()
#2 {main}
  thrown in [chronos path]/src/Chronos/User.php on line 90
*/

```

### Request Modifiers

#### Overview

Modifiers are designed to make it easier to define and overwrite default request options (e.g. query string parameters, post data, headers, etc.)

The request options array will pass through the following sequence:

```php
<?php

use \LapetusSolutions\Chronos;

$chronos = new Chronos\Chronos([
    "apiroot" => "https://chronos.lapetussolutions.com/v1",
    "orgid" => "[orgid]",
    "appid" => "[appid]",
    "apikey" => "[apikey]",
]);

$request_options = [
    "params" => [],
    "data" => [],
    "headers" => [],
];

$options = [
    "modifiers" => [
        "pre" => [],
        "post" => [],
    ],
    "override_modifiers" => false,
];

$instance->request($request_options, $options);

```

The `$request_options` parameter will be passed through every modifier in the following arrays:

- `$instance->modifiers["pre"]`
- `$instance->modifiers["post"]`
- `$options["modifiers"]["pre"]`
- `$options["modifiers"]["post"]`

If `$options["override_modifiers"]` is `true`, the `$request_options` parameter will only be passed through the modifiers in `$options["modifiers"]`

#### Define your own modifier

Modifiers can be defined in two ways. Here are two examples of how you could define a modifier to add headers to a request:

```php
<?php

use \LapetusSolutions\Chronos;

$additional_headers = [
    "Some-Header" => "Some-Value",
];

class SetAdditionalHeadersModifier extends Chronos\RequestModifier {
    public function requestOptions($instance, &$request_options, $options = []) {
        global $additional_headers;
        if (!isset($request_options["headers"])) {
            $request_options["headers"] = [];
        }
        foreach ($additional_headers as $key => $value) {
            $request_options["headers"][$key] = $value;
        }
    }
}
function set_additional_headers_modifier($instance, &$request_options, $options = []) {
    global $additional_headers;
    if (!isset($request_options["headers"])) {
        $request_options["headers"] = [];
    }
    foreach ($additional_headers as $key => $value) {
        $request_options["headers"][$key] = $value;
    }
}
```

#### Add a default modifier

To have a modifier be applied to every request in a `Chronos` or `User` instance, add the modifier to the appropriate array in the `modifiers` property

```php
<?php

$chronos->modifiers["pre"][] = new SetAdditionalHeadersModifier();
$user->modifiers["pre"][] = new SetAdditionalHeadersModifier();

// or

$chronos->modifiers["pre"][] = "\set_additional_headers_modifier";
$user->modifiers["pre"][] = "\set_additional_headers_modifier";

```