|ProjectName| for Chronos |ProjectVersion|
================================================

.. toctree::
   installation
   examples
   documentation

* :ref:`genindex`
* :ref:`search`