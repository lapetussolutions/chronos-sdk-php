## Installation

### With Composer (recommended)

Dependencies:

- [Composer](https://github.com/composer/composer)
- PHP Version 5.3.2 or above
- composer.json in the root of your project

To add the repository and require the package, run these commands in the root of your project:

```sh
composer config repositories.lapetussolutions-chronos vcs https://bitbucket.org/lapetussolutions/chronos-sdk-php.git
composer require lapetussolutions/chronos:v1.0.0
```

Include the composer autoloader at the top of your PHP file:
```php
<?php

require("vendor/autoload.php");
```

### From Source

Dependencies

- PHP Version 5.3 or above

#### Download

- Clone with Git:

    ```sh
    git clone https://bitbucket.org/lapetussolutions/chronos-sdk-php.git
    cd chronos-sdk-php
    git checkout tags/v1.0.0
    cd ..
    ```

- or, [download tag directly](https://bitbucket.org/lapetussolutions/chronos-sdk-php/downloads/?tab=tags)

#### Include package from local directory

```php
<?php

require("path/to/chronos-sdk-php/include.php");
```
