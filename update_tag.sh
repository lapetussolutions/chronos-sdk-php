#!/bin/sh

tag=$1

git tag -d $1 && git push origin :$1
git tag -a $1 -m "Updated $1 tag" && git push origin $1
